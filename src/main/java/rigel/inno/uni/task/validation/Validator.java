package rigel.inno.uni.task.validation;

/**
 * И
 * @param <T>
 */
public interface Validator<T>{
    boolean validate(T v);
}
