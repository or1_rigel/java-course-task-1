package rigel.inno.uni.task.validation;

import java.util.regex.Pattern;

public class CyrillicValidator implements Validator<String> {
    private static final String cyrillicPattern = "^[а-яА-ЯёЁ0-9\\d]+$";
    private static final Pattern pattern = Pattern.compile(cyrillicPattern);

    @Override
    public boolean validate(String word) {
        return pattern.matcher(word).matches();
    }
}
