package rigel.inno.uni.task.services;


import org.apache.commons.cli.*;

public class CommandLineService {
    private static Options options = null;

    /**
     * Парсит аргументы командной строки
     * @param args
     * @return
     * @throws ParseException
     */
    public static CommandLine getCommandLine(String[] args) throws ParseException {
        if(options == null) {
            options = constructOptions();
        }

        CommandLineParser parser = new DefaultParser();
        CommandLine line = parser.parse(options, args);
        return line;
    }

    /**
     * Выводит подсказку для пользователя на экран
     * @param toolName
     */
    public static void printHelp(String toolName) {
        if(options == null) {
            options = constructOptions();
        }

        new HelpFormatter().printHelp(toolName, options);
    }

    /**
     * Инициализация параметров, которые будут обрабатываться из
     * командной строки
     * @return
     */
    private static Options constructOptions() {
        Options options = new Options();

        Option fileOption = Option.builder("f")
                .longOpt("files")
                .argName("file")
                .hasArgs()
                .desc("input files")
                .build();

        Option urlOption = Option.builder("u")
                    .longOpt("urls")
                    .argName("url")
                    .hasArgs()
                    .desc("input urls")
                    .build();

        Option helpOption = new Option("help", false, "help");

        options.addOption(fileOption);
        options.addOption(urlOption);
        options.addOption(helpOption);



        return options;
    }
}
