package rigel.inno.uni.task.resources;

import java.io.InputStream;

public interface Resource {
    /**
     * Ресурс должен возвращать поток для чтения
     * @return поток
     * @throws Exception
     */
    InputStream getStream() throws Exception;
}
