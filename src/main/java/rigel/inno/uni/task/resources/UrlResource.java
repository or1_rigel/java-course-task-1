package rigel.inno.uni.task.resources;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Ресурс, который читает по url
 */
public class UrlResource implements Resource {
    private static final Logger logger = LoggerFactory.getLogger(UrlResource.class);

    private URL url;

    public UrlResource(URL url) {
        this.url = url;
    }

    public UrlResource(String url) throws MalformedURLException {
        this.url = new URL(url);
    }


    @Override
    public InputStream getStream() throws IOException {
        return new BufferedInputStream(url.openStream());
    }

    @Override
    public String toString() {
        return "UrlResource: " + url.toString();
    }
}
