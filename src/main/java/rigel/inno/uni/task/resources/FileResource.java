package rigel.inno.uni.task.resources;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

/**
 * Ресурс, который читает из файла
 */
public class FileResource implements Resource {
    private static final Logger logger = LoggerFactory.getLogger(FileResource.class);

    private File file;

    public FileResource(File file) throws FileNotFoundException {
        if(!file.exists() || file.isDirectory()) {
            throw new FileNotFoundException();
        }

        this.file = file;

    }

    public FileResource(String filePath) throws FileNotFoundException {
        this(new File(filePath));
    }

    @Override
    public InputStream getStream() throws FileNotFoundException {
        return new FileInputStream(file);
    }

    @Override
    public String toString() {
        return "FileResource: " + file;
    }
}
