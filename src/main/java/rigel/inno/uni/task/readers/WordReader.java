package rigel.inno.uni.task.readers;


public interface WordReader extends AutoCloseable {
    /**
     * @return слово из потока
     */
    String nextWord();
}
