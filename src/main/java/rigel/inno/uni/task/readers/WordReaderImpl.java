package rigel.inno.uni.task.readers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FilterReader;
import java.io.IOException;
import java.io.Reader;

/**
 * Считывает слова из потока
 */

public class WordReaderImpl extends FilterReader implements WordReader {

    private static final Logger logger = LoggerFactory.getLogger(WordReaderImpl.class);

    private final int BUFFER_SIZE = 64;
    private char[] separators;

    public WordReaderImpl(Reader in, char[] separators) {
        super(in);
        this.separators = separators;
    }

    /**
     * Метод читает посимвольно из потока в буфер,
     * выделяя слова по символам разделителям
     * @return следующие слово из потока
     */
    public String nextWord() {
        char[] wordBuffer;
        char[] buf = wordBuffer = new char[BUFFER_SIZE];
        int room = BUFFER_SIZE;
        int end = 0;
        try {
            while (true) {
                int c = in.read();

                if(c == STREAM_END) {
                   return null;
                }

                if(!isSeparator(c)) {
                    buf[end++] = (char) c;
                    --room;
                    while(true) {
                        c = in.read();
                        if(isSeparator(c) || c == STREAM_END) {
                            return String.copyValueOf(buf, 0, end);
                        }

                        buf[end++] = (char) c;
                        --room;

                        if(room == 0) {
                            buf = new char[end + BUFFER_SIZE];
                            room = BUFFER_SIZE;
                            System.arraycopy(wordBuffer, 0, buf, 0, end);
                            wordBuffer = buf;
                        }
                    }
                }
            }

        } catch (IOException e) {
            logger.error(e.getMessage());
        }

        return null;
    }

    /**
     * Метод проверяет является ли символ разделителем
     * @param s символ для проверки
     * @return если символ является разделителем, то
     *         возвращается true, иначе false
     */
    private boolean isSeparator(int s) {
        for (char separator : separators) {
            if (separator == (char) s)
                return true;
        }

        return false;
    }

    public void close() throws IOException {
        in.close();
    }

    private final int STREAM_END = -1;

}
