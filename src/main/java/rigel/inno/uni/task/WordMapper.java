package rigel.inno.uni.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rigel.inno.uni.task.readers.WordReaderImpl;
import rigel.inno.uni.task.readers.WordReader;
import rigel.inno.uni.task.resources.Resource;
import rigel.inno.uni.task.validation.ValidationException;
import rigel.inno.uni.task.validation.Validator;

import java.io.*;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Обработчик считанных слов из потока
 */
public class WordMapper implements Runnable {
    private static final Logger logger = LoggerFactory.getLogger(WordMapper.class);


    private SharedThreadResources sharedResources;
    private Resource resource;
    private Validator validator;
    private char[] separators;

    private Lock lock = new ReentrantLock();

    /**
     * Класс - поток, который считывает из потока слова с помощью WordReader,
     * проверяет их на правильность и добавляет в общее множество.
     * @param resource ресурс из которого идет чтение
     * @param shared общие переменные для потоков
     * @param validator проверка слов
     * @see WordReader
     */
    public WordMapper(Resource resource, SharedThreadResources shared, Validator validator) {
        this(resource, shared, validator, new char[] {' '});
    }

    /**
     * Класс - поток, который считывает из потока слова с помощью WordReader,
     * проверяет их на правильность и добавляет в общее множество.
     * @param resource ресурс из которого идет чтение
     * @param shared общие переменные для потоков
     * @param validator проверка слов
     * @param separators список разделителей
     * @see WordReader
     */
    public WordMapper(Resource resource, SharedThreadResources shared, Validator validator, char[] separators) {
        this.resource = resource;
        this.sharedResources = shared;
        this.validator = validator;
        this.separators = separators;
    }


    @Override
    public void run() {
        logger.info("Start read words");
        try(WordReader reader = new WordReaderImpl(
                new InputStreamReader(resource.getStream()),
                separators)
        ) {
            Thread t = Thread.currentThread();
            String word = null;
            while(((word = reader.nextWord()) != null) && !t.isInterrupted()) {
                synchronized (sharedResources) {

                    if(sharedResources.isEnd) {
                        return;
                    }

                    if (!validator.validate(word)) {

                        System.out.println("Bad input");

                        ValidationException e = new ValidationException();
                        logger.error("word validation error: {}. End search", word, e);

                        // Останавливаем поток
                        sharedResources.isEnd = true;
                        return;
                    }

                    if (sharedResources.set.contains(word)) {
                        logger.info("Find duplicate {}", word);

                        System.out.println("Duplicate: " + word);

                        // Останавливаем поток
                        sharedResources.isEnd = true;
                        sharedResources.duplicate = true;
                        return;
                    }

                    sharedResources.set.add(word);
                }
            }

        } catch (Exception e) {
            logger.error("Can't read resource: {}", resource.toString(), e);
            sharedResources.isEnd = true;
            return;
        }
    }
}
