package rigel.inno.uni.task;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rigel.inno.uni.task.resources.FileResource;
import rigel.inno.uni.task.resources.Resource;
import rigel.inno.uni.task.resources.UrlResource;
import rigel.inno.uni.task.services.CommandLineService;
import rigel.inno.uni.task.validation.CyrillicValidator;

import java.io.FileNotFoundException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

public class Main {

    private static Logger logger = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {

        CommandLine line;
        try {
           line = CommandLineService.getCommandLine(args);

           if(line.hasOption("help") || line.getOptions().length == 0) {
               CommandLineService.printHelp("words");
               return;
           }

        } catch (ParseException e) {
            logger.error("parsing error. print help");
            CommandLineService.printHelp("words");
            return;
        }


        List<Resource> resources = collectResources(line);

        if(resources == null || resources.size() == 0) {
            logger.warn("empty resource collection");
            return;
        }

        SharedThreadResources shared = new SharedThreadResources();
        List<Thread> threads = createThreadsFromResources(resources, shared);

        startThreads(threads);
        joinThreads(threads);


        if(!shared.duplicate)  {
           logger.info("Not found");
           System.out.println("Not found");
        }
    }


    private static List<Resource> collectResources(CommandLine line) {

        List<Resource> resources = new ArrayList<>();

        try {
            // files
            if(line.hasOption('f')) {
                for(String filePath : line.getOptionValues('f')) {
                    resources.add(new FileResource(filePath));
                }
            }

            // url
            if(line.hasOption('u')) {
                for (String url : line.getOptionValues('u')) {
                    resources.add(new UrlResource(url));
                }
            }
        } catch (FileNotFoundException e) {
            logger.error("Bad file", e);
            return null;
        } catch (MalformedURLException e) {
            logger.error("Bad url", e);
            return null;
        }

        return resources;
    }


    private static void startThreads(List<Thread> threads) {
        for(Thread t : threads) {
            t.start();
        }
    }


    private static void joinThreads(List<Thread> threads) {
        try {
            for(Thread t : threads) {
                t.join();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    private static List<Thread> createThreadsFromResources(List<Resource> resources, SharedThreadResources shared) {
        List<Thread> threads = new ArrayList<>();

        for(Resource resource : resources) {

            WordMapper mapper = new WordMapper(resource,
                    shared,
                    new CyrillicValidator(),
                    new char[] {' ', ',', '.', '\n', '(', ')', '!', ':', ';', '?' } // Символы разделители
            );

            threads.add(new Thread(mapper));
        }

        return threads;
    }

}
