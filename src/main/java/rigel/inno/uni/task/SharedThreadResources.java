package rigel.inno.uni.task;


import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Общий ресурс для потоков
 */
public class SharedThreadResources {

    /**
     * Множество, которое хранит считаные слова
     */
    public volatile Set<String> set;

    /**
     * Флаг остановки потоков
     */
    public volatile boolean isEnd;

    /**
     * Слово
     */
    public volatile boolean duplicate;

    public SharedThreadResources() {
        this.set = Collections.newSetFromMap(new ConcurrentHashMap<>());
        this.duplicate = false;
        this.isEnd = false;
    }
}

