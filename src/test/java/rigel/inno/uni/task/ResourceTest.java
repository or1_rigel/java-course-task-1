package rigel.inno.uni.task;

import org.junit.Test;
import rigel.inno.uni.task.resources.FileResource;
import rigel.inno.uni.task.resources.UrlResource;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.MalformedURLException;

public class ResourceTest {
    @Test(expected = FileNotFoundException.class)
    public void badFile() throws Exception {
        String filePath = "fileBadPath";
        FileResource resource = new FileResource(filePath);
    }

    @Test(expected = MalformedURLException.class)
    public void badUrl() throws Exception {
        String url = "com.http://";
        UrlResource resource =  new UrlResource(url);
    }
}
