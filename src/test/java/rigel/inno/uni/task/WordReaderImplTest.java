package rigel.inno.uni.task;

import org.junit.Test;
import rigel.inno.uni.task.readers.WordReaderImpl;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;


public class WordReaderImplTest {
    @Test
    public void simpleInput() {
        String inputString = "жираф кот";
        InputStream is = new ByteArrayInputStream(inputString.getBytes());

        WordReaderImpl reader = new WordReaderImpl(
                new InputStreamReader(is),
                new char[] {' ', ','}

        );

        String one = reader.nextWord();
        String two = reader.nextWord();
        assertEquals(one, "жираф");
        assertEquals(two, "кот");
    }

    @Test
    public void emptyInput() {
        String inputString = "";
        InputStream is = new ByteArrayInputStream(inputString.getBytes());

        WordReaderImpl reader = new WordReaderImpl(
                new InputStreamReader(is),
                new char[] {' ', ','}
        );

        assertNull(reader.nextWord());
    }


    @Test
    public void manySeparators() {
        String inputString = "кот крот,       манго...,кашелот111 123 (кайот) и,б\nпилот";
        InputStream is = new ByteArrayInputStream(inputString.getBytes());

        WordReaderImpl reader = new WordReaderImpl(
                new InputStreamReader(is),
                new char[] {' ', ',', '.', '(', ')', '\n'}
        );

        assertEquals(reader.nextWord(), "кот");
        assertEquals(reader.nextWord(), "крот");
        assertEquals(reader.nextWord(), "манго");
        assertEquals(reader.nextWord(), "кашелот111");
        assertEquals(reader.nextWord(), "123");
        assertEquals(reader.nextWord(), "кайот");
        assertEquals(reader.nextWord(), "и");
        assertEquals(reader.nextWord(), "б");
        assertEquals(reader.nextWord(), "пилот");
    }
}