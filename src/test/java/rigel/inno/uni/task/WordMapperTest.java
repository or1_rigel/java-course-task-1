package rigel.inno.uni.task;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

import rigel.inno.uni.task.resources.FileResource;
import rigel.inno.uni.task.resources.Resource;
import rigel.inno.uni.task.validation.CyrillicValidator;

import java.io.ByteArrayInputStream;

public class WordMapperTest {
    @Test
    public void goodInput() throws Exception {

        SharedThreadResources shared = new SharedThreadResources();

        Resource resource = mock(FileResource.class);
        String input = "кот черный";
        when(resource.getStream()).thenReturn(new ByteArrayInputStream(input.getBytes()));

        WordMapper mapper = new WordMapper(resource, shared, new CyrillicValidator());

        mapper.run();

        assertEquals(shared.isEnd, false);
        assertEquals(shared.duplicate, null);
    }

    @Test
    public void badInput() throws Exception {
        SharedThreadResources shared = new SharedThreadResources();

        Resource resource = mock(FileResource.class);

        String input = "котШY прк";
        when(resource.getStream()).thenReturn(new ByteArrayInputStream(input.getBytes()));

        WordMapper mapper = new WordMapper(resource, shared, new CyrillicValidator());

        mapper.run();

        assertEquals(shared.isEnd, true);
        assertEquals(shared.duplicate, null);
    }


    @Test
    public void duplicateInput() throws Exception {
        SharedThreadResources shared = new SharedThreadResources();

        Resource resource = mock(FileResource.class);

        String input = "кот собака кот";
        when(resource.getStream()).thenReturn(new ByteArrayInputStream(input.getBytes()));

        WordMapper mapper = new WordMapper(resource, shared, new CyrillicValidator());

        mapper.run();

        assertEquals(shared.isEnd, true);
        assertEquals(shared.duplicate, "кот");
    }

}
