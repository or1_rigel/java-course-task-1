package rigel.inno.uni.task;

import org.junit.Test;
import static org.junit.Assert.*;
import rigel.inno.uni.task.validation.CyrillicValidator;
import rigel.inno.uni.task.validation.Validator;

public class CyrillicValidatorTest {

    private Validator validator = new CyrillicValidator();

    @Test
    public void valid() {
        String str = "ЗОВьчыГЫВьмывафыщЛЛЛл";
        assertTrue(validator.validate(str));
    }

    @Test
    public void notCyrillic() {
       String str = "ОВЛЫщьBBBddk";
       assertFalse(validator.validate(str));
    }

    @Test
    public void withSeparator() {
        String str = ",,,,слово    ";
        assertFalse(validator.validate(str));
    }


    @Test
    public void withNumbers() {
        String str = "123";
        assertTrue(validator.validate(str));
    }


    @Test
    public void oneSymbol() {
       String str = "и";
       assertTrue(validator.validate(str));
    }

}
