# README


```
#!bash

./gradlew build
```


```
#!bash

usage: words

 -f,--files <file>   input files
 -help               help
 -u,--urls <url>     input urls
```

Example:

```
#!bash

java -jar task-one-0.1.0-SNAPSHOT.jar -f files/0-words files/1-words
```